package be.sourcedbvba.kali.domain.event

interface EventPublisher {
    fun publishEvent(event: Any)

    object Locator {
        lateinit var eventPublisher: EventPublisher
    }
}