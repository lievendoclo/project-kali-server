package be.sourcedbvba.kali.domain.event

import be.sourcedbvba.kali.domain.entity.Floor

data class FloorSaved(val floor: Floor) : DomainEvent
