package be.sourcedbvba.kali.domain.entity

import be.sourcedbvba.kali.context.CompanyContext
import be.sourcedbvba.kali.domain.event.ProductSaved
import be.sourcedbvba.kali.identifier.CompanyId
import be.sourcedbvba.kali.identifier.ProductId
import java.util.*

class Product(val id: ProductId,
              val name: String) {
    companion object {
        fun createNew(name: String) : Product {
            val id = UUID.randomUUID().toString()
            return Product(ProductId(id), name)
        }
    }

    fun save() : Product {
        ProductSaved(this).sendEvent()
        return this
    }

    override fun toString(): String {
        return "Product(id=$id, name='$name')"
    }


}