package be.sourcedbvba.kali.domain.event

import be.sourcedbvba.kali.domain.entity.PriceList

data class PriceListSaved(val priceList: PriceList) : DomainEvent
