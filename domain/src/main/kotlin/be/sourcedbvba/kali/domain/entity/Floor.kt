package be.sourcedbvba.kali.domain.entity

import be.sourcedbvba.kali.context.CompanyContext
import be.sourcedbvba.kali.domain.event.FloorSaved
import be.sourcedbvba.kali.domain.event.TableAddedToFloor
import be.sourcedbvba.kali.identifier.CompanyId
import be.sourcedbvba.kali.identifier.FloorId
import java.util.*

class Floor(val id: FloorId,
            val name: String,
            private val _tables: MutableSet<Table>) {
    companion object {
        fun createNew(name: String): Floor {
            val id = UUID.randomUUID().toString()
            return Floor(FloorId(id), name, mutableSetOf())
        }
    }

    val tables: Set<Table> = _tables

    fun save(): Floor {
        FloorSaved(this).sendEvent()
        return this
    }

    fun addTable(table: Table): Floor {
        _tables.add(table)
        TableAddedToFloor(this.id, table.id).sendEvent()
        return this
    }

    override fun toString(): String {
        return "Floor(id='$id', name='$name', tables=$tables)"
    }
}


