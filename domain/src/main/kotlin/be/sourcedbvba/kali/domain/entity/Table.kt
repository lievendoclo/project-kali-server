package be.sourcedbvba.kali.domain.entity

import be.sourcedbvba.kali.context.CompanyContext
import be.sourcedbvba.kali.domain.event.TableSaved
import be.sourcedbvba.kali.identifier.CompanyId
import be.sourcedbvba.kali.identifier.TableId
import java.util.*

class Table(val id: TableId,
            val name: String) {
    companion object {
        fun createNew(name: String): Table {
            val id = UUID.randomUUID().toString()
            return Table(TableId(id), name)
        }
    }

    fun save() : Table {
        TableSaved(this).sendEvent()
        return this
    }

    override fun toString(): String {
        return "Table(id=$id, name='$name')"
    }


}

