package be.sourcedbvba.kali.domain.event

import be.sourcedbvba.kali.identifier.ReceiptId

data class ProFormaReceiptRequested(val receiptId: ReceiptId)