package be.sourcedbvba.kali.domain.entity

import be.sourcedbvba.kali.context.CompanyContext
import be.sourcedbvba.kali.domain.event.PriceListSaved
import be.sourcedbvba.kali.identifier.CompanyId
import be.sourcedbvba.kali.identifier.PriceListId
import be.sourcedbvba.kali.identifier.ProductId
import java.util.*
import javax.money.CurrencyUnit
import javax.money.MonetaryAmount

class PriceList(val id: PriceListId,
                val name: String,
                val currency: CurrencyUnit,
                val prices: Map<ProductId, MonetaryAmount>) {
    companion object {
        fun createNew(name: String, currency: CurrencyUnit): PriceList {
            val id = UUID.randomUUID().toString()
            return PriceList(PriceListId(id), name, currency, mapOf())
        }
    }

    fun save(): PriceList {
        PriceListSaved(this).sendEvent()
        return this
    }

    override fun toString(): String {
        return "PriceList(id=$id, name='$name', currency=$currency, prices=$prices)"
    }


}