package be.sourcedbvba.kali.domain.event

import be.sourcedbvba.kali.identifier.PriceListId
import be.sourcedbvba.kali.identifier.ProductId
import javax.money.MonetaryAmount

data class PriceSetForProduct(val priceListId: PriceListId,
                              val productId: ProductId,
                              val price: MonetaryAmount)