package be.sourcedbvba.kali.domain.event

import be.sourcedbvba.kali.identifier.ProductId
import be.sourcedbvba.kali.identifier.ReceiptId
import javax.money.MonetaryAmount

data class ReceiptSaved(val id: ReceiptId,
                        val receiptNumber: String) {
    data class ReceiptItem(val productId: ProductId,
                           val quantity: Int,
                           val price: MonetaryAmount)
}