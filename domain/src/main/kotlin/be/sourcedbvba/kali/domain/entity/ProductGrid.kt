package be.sourcedbvba.kali.domain.entity

import be.sourcedbvba.kali.identifier.CompanyId
import be.sourcedbvba.kali.identifier.ProductGridId
import be.sourcedbvba.kali.identifier.ProductId

class ProductGrid(val id: ProductGridId,
                  val name: String,
                  val rows: Int,
                  val columns: Int,
                  val products: Map<Pair<Int, Int>, ProductId>) {
    override fun toString(): String {
        return "ProductGrid(id=$id, name='$name', rows=$rows, columns=$columns, products=$products)"
    }
}
