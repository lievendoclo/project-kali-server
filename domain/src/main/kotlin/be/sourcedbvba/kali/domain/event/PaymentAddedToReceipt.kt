package be.sourcedbvba.kali.domain.event

import be.sourcedbvba.kali.enums.PaymentType
import be.sourcedbvba.kali.identifier.ReceiptId
import javax.money.MonetaryAmount

data class PaymentAddedToReceipt(val receiptId: ReceiptId,
                                 val paymentType: PaymentType,
                                 val amount: MonetaryAmount) : DomainEvent