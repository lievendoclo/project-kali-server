package be.sourcedbvba.kali.domain.event

import be.sourcedbvba.kali.domain.entity.User

data class UserSaved(val user: User) : DomainEvent
