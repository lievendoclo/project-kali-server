package be.sourcedbvba.kali.domain.entity

import be.sourcedbvba.kali.enums.ReceiptStatus
import be.sourcedbvba.kali.identifier.CompanyId
import be.sourcedbvba.kali.identifier.ProductId
import be.sourcedbvba.kali.identifier.ReceiptId
import be.sourcedbvba.kali.identifier.TableId
import javax.money.MonetaryAmount

class Receipt(val id: ReceiptId,
              val receiptNumber: String = "",
              val status: ReceiptStatus,
              val tableId: TableId,
              val receiptItems: List<ReceiptItem>) {

    class ReceiptItem(val productId: ProductId,
                      val quantity: Int,
                      val price: MonetaryAmount) {
        override fun toString(): String {
            return "ReceiptItem(productId=$productId, quantity=$quantity, price=$price)"
        }
    }

    override fun toString(): String {
        return "Receipt(id=$id, receiptNumber='$receiptNumber', status=$status, tableId=$tableId, receiptItems=$receiptItems)"
    }


}