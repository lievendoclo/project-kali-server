package be.sourcedbvba.kali.domain.entity

import be.sourcedbvba.kali.identifier.CompanyId

class Company(val id: CompanyId,
              val name: String) {
}