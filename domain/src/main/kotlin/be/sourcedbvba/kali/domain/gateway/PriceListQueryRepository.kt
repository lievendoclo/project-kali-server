package be.sourcedbvba.kali.domain.gateway

import be.sourcedbvba.kali.domain.entity.PriceList
import be.sourcedbvba.kali.identifier.PriceListId

interface PriceListQueryRepository {
    fun findAllPriceLists(): List<PriceList>
    fun getPriceList(id: PriceListId): PriceList
}