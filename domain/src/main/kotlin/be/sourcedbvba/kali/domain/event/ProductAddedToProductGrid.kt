package be.sourcedbvba.kali.domain.event

import be.sourcedbvba.kali.identifier.ProductGridId
import be.sourcedbvba.kali.identifier.ProductId

data class ProductAddedToProductGrid(val productGridId: ProductGridId,
                                     val productId: ProductId,
                                     val row: Int,
                                     val column: Int) : DomainEvent
