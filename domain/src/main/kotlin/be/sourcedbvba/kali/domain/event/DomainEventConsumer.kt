package be.sourcedbvba.kali.domain.event

interface DomainEventConsumer<E : DomainEvent> {
    fun consume(event: E)
}