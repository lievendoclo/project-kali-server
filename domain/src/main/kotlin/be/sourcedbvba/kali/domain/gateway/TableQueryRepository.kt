package be.sourcedbvba.kali.domain.gateway

import be.sourcedbvba.kali.domain.entity.Table
import be.sourcedbvba.kali.identifier.TableId

interface TableQueryRepository {
    fun getTable(id: TableId) : Table
}
