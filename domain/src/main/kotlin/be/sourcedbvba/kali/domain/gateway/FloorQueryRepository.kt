package be.sourcedbvba.kali.domain.gateway

import be.sourcedbvba.kali.domain.entity.Floor
import be.sourcedbvba.kali.identifier.FloorId

interface FloorQueryRepository {
    fun findAllFloors(): Iterable<Floor>
    fun getFloorById(id: FloorId): Floor
}