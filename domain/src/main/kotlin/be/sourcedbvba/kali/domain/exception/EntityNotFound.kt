package be.sourcedbvba.kali.domain.exception

class EntityNotFound : RuntimeException()
