package be.sourcedbvba.kali.domain.event

import be.sourcedbvba.kali.domain.entity.Product

data class ProductSaved(val product: Product) : DomainEvent
