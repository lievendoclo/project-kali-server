package be.sourcedbvba.kali.domain.event

import be.sourcedbvba.kali.domain.entity.Table

data class TableSaved(val table: Table) : DomainEvent