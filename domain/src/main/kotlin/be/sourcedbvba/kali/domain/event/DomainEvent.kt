package be.sourcedbvba.kali.domain.event

interface DomainEvent {
    fun sendEvent() {
        EventPublisher.Locator.eventPublisher.publishEvent(this)
    }
}