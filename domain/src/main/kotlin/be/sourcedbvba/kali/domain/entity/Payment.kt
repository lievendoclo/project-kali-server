package be.sourcedbvba.kali.domain.entity

import be.sourcedbvba.kali.enums.PaymentType
import be.sourcedbvba.kali.identifier.CompanyId
import be.sourcedbvba.kali.identifier.PaymentId
import be.sourcedbvba.kali.identifier.ReceiptId
import javax.money.MonetaryAmount

class Payment(val id: PaymentId,
              val receiptId: ReceiptId,
              val paymentType: PaymentType,
              val amount: MonetaryAmount) {

    override fun toString(): String {
        return "Payment(id=$id, receiptId=$receiptId, paymentType=$paymentType, amount=$amount)"
    }
}