package be.sourcedbvba.kali.domain.event

import be.sourcedbvba.kali.identifier.ReceiptId

data class ReceiptFinalized(val receiptId: ReceiptId) : DomainEvent