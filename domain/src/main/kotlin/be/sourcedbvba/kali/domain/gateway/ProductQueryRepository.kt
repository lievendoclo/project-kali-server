package be.sourcedbvba.kali.domain.gateway

import be.sourcedbvba.kali.domain.entity.Product
import be.sourcedbvba.kali.identifier.ProductId

interface ProductQueryRepository {
    fun findProducts(query: FindProductsQuery) : List<Product>
    fun getProduct(id: ProductId): Product
}

data class FindProductsQuery(val nameContains: String?)