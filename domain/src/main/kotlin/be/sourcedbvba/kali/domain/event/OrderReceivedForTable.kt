package be.sourcedbvba.kali.domain.event

import be.sourcedbvba.kali.identifier.ProductId
import be.sourcedbvba.kali.identifier.TableId

data class OrderReceivedForTable(val tableId: TableId,
                            val orderItems: List<OrderItem>) : DomainEvent {
    data class OrderItem(val productId: ProductId,
                         val amount: Int)
}