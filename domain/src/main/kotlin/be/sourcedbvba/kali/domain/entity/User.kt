package be.sourcedbvba.kali.domain.entity

import be.sourcedbvba.kali.domain.event.UserSaved
import be.sourcedbvba.kali.identifier.UserId
import java.util.*

class User(val id: UserId,
           val name: String,
           val username: String,
           val password: String) {
    companion object {
        fun createNew(name: String, username: String, password: String): User {
            val id = UUID.randomUUID().toString()
            return User(UserId(id), name, username, password)
        }
    }

    fun save(): User {
        UserSaved(this).sendEvent()
        return this
    }

    override fun toString(): String {
        return "User(id=$id, name='$name', username='$username', password='$password')"
    }


}