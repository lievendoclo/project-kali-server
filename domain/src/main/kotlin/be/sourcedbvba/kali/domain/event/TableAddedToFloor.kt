package be.sourcedbvba.kali.domain.event

import be.sourcedbvba.kali.identifier.FloorId
import be.sourcedbvba.kali.identifier.TableId

data class TableAddedToFloor(val floorId: FloorId, val tableId: TableId) : DomainEvent
