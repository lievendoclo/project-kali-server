package be.sourcedbvba.kali.domain.event

import be.sourcedbvba.kali.identifier.ProductGridId

data class ProductGridCreated(val id: ProductGridId,
                              val name: String,
                              val rows: Int,
                              val columns: Int) : DomainEvent