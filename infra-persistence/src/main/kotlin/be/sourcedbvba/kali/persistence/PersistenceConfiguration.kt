package be.sourcedbvba.kali.persistence

import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@Configuration
@EnableJpaRepositories
@EntityScan("be.sourcedbvba.kali.persistence.jpa.mapping")
@ComponentScan
class PersistenceConfiguration {
}