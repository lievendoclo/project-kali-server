package be.sourcedbvba.kali.persistence.jpa.eventhandler

import be.sourcedbvba.kali.domain.event.DomainEventConsumer
import be.sourcedbvba.kali.domain.event.FloorSaved
import be.sourcedbvba.kali.persistence.jpa.eventhandler.annotation.EventHandler
import be.sourcedbvba.kali.persistence.jpa.mapping.toEntity
import be.sourcedbvba.kali.persistence.jpa.repository.FloorEntityRepository

@EventHandler
class FloorSavedHandler(val floorEntityRepository: FloorEntityRepository) : DomainEventConsumer<FloorSaved> {
    override fun consume(event: FloorSaved) {
        val floorDocument = event.floor.toEntity()
        floorEntityRepository.save(floorDocument)
    }
}
