package be.sourcedbvba.kali.persistence.jpa.repository

import be.sourcedbvba.kali.persistence.jpa.mapping.TableEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.querydsl.QuerydslPredicateExecutor

interface TableEntityRepository : JpaRepository<TableEntity, String>, QuerydslPredicateExecutor<TableEntity>