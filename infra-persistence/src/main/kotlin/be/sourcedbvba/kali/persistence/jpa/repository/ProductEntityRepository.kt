package be.sourcedbvba.kali.persistence.jpa.repository

import be.sourcedbvba.kali.persistence.jpa.mapping.ProductEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.querydsl.QuerydslPredicateExecutor

interface ProductEntityRepository : JpaRepository<ProductEntity, String>, QuerydslPredicateExecutor<ProductEntity> {
}