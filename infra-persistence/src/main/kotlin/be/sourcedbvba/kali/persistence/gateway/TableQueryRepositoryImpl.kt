package be.sourcedbvba.kali.persistence.gateway

import be.sourcedbvba.kali.context.CompanyContext
import be.sourcedbvba.kali.domain.entity.Table
import be.sourcedbvba.kali.domain.exception.EntityNotFound
import be.sourcedbvba.kali.domain.gateway.TableQueryRepository
import be.sourcedbvba.kali.identifier.TableId
import be.sourcedbvba.kali.persistence.jpa.mapping.QTableEntity
import be.sourcedbvba.kali.persistence.jpa.repository.TableEntityRepository
import org.springframework.stereotype.Repository

@Repository
class TableQueryRepositoryImpl(private val tableEntityRepository: TableEntityRepository) : TableQueryRepository {
    private val TABLE = QTableEntity.tableEntity

    override fun getTable(id: TableId): Table {
        fun withId(id: TableId) = TABLE.id.eq(id.value)
        return tableEntityRepository.findOne(forCurrentCompany().and(withId(id)))
                .orElseThrow { EntityNotFound() }
                .toDomain()
    }

    fun forCurrentCompany() = TABLE.companyId.eq(CompanyContext.companyId.value)

}
