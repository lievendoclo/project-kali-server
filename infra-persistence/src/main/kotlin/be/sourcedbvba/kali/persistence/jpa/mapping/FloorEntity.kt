package be.sourcedbvba.kali.persistence.jpa.mapping

import be.sourcedbvba.kali.context.CompanyContext
import be.sourcedbvba.kali.domain.entity.Floor
import be.sourcedbvba.kali.identifier.FloorId
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToMany

@Entity(name = "Floor")
data class FloorEntity(@Id val id: String,
                       val companyId: String,
                       val name: String,
                       @OneToMany @JoinColumn(name = "floorId") val tables: MutableSet<TableEntity>) {
    internal constructor(floor: Floor) : this(floor.id.value, CompanyContext.companyId.value, floor.name, floor.tables.map { it.toEntity() }.toMutableSet())

    fun toDomain(): Floor {
        return Floor(FloorId(id), name, tables.map { it.toDomain() }.toMutableSet())
    }
}

fun Floor.toEntity() : FloorEntity {
    return FloorEntity(this);
}



