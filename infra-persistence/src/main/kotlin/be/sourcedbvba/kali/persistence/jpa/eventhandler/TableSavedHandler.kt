package be.sourcedbvba.kali.persistence.jpa.eventhandler

import be.sourcedbvba.kali.domain.event.DomainEventConsumer
import be.sourcedbvba.kali.domain.event.TableSaved
import be.sourcedbvba.kali.persistence.jpa.eventhandler.annotation.EventHandler
import be.sourcedbvba.kali.persistence.jpa.mapping.toEntity
import be.sourcedbvba.kali.persistence.jpa.repository.TableEntityRepository

@EventHandler
class TableSavedHandler(val tableEntityRepository: TableEntityRepository) : DomainEventConsumer<TableSaved> {
    override fun consume(event: TableSaved) {
        tableEntityRepository.save(event.table.toEntity())
    }
}