package be.sourcedbvba.kali.persistence.jpa.repository

import be.sourcedbvba.kali.persistence.jpa.mapping.PriceListEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.querydsl.QuerydslPredicateExecutor

interface PriceListEntityRepository : JpaRepository<PriceListEntity, String>, QuerydslPredicateExecutor<PriceListEntity>