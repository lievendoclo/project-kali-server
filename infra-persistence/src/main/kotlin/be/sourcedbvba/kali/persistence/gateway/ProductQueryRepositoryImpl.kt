package be.sourcedbvba.kali.persistence.gateway

import be.sourcedbvba.kali.context.CompanyContext
import be.sourcedbvba.kali.domain.entity.Product
import be.sourcedbvba.kali.domain.exception.EntityNotFound
import be.sourcedbvba.kali.domain.gateway.FindProductsQuery
import be.sourcedbvba.kali.domain.gateway.ProductQueryRepository
import be.sourcedbvba.kali.identifier.ProductId
import be.sourcedbvba.kali.persistence.jpa.mapping.QProductEntity
import be.sourcedbvba.kali.persistence.jpa.repository.ProductEntityRepository
import com.querydsl.core.BooleanBuilder
import org.springframework.stereotype.Repository

@Repository
class ProductQueryRepositoryImpl(private val productEntityRepository: ProductEntityRepository) : ProductQueryRepository {
    private val PRODUCT = QProductEntity.productEntity

    override fun getProduct(id: ProductId): Product {
        fun withId(id: ProductId) = PRODUCT.id.eq(id.value)
        return productEntityRepository.findOne(forCurrentCompany().and(withId(id)))
                .orElseThrow { EntityNotFound() }
                .toDomain()
    }

    override fun findProducts(query: FindProductsQuery): List<Product> {
        fun nameContains(value: String) = QProductEntity.productEntity.name.containsIgnoreCase(value)
        val filter = BooleanBuilder()
        filter.and(forCurrentCompany())
        query.nameContains?.let { filter.and(nameContains(it)) }

        return productEntityRepository.findAll(filter)
                .map { it.toDomain() }
    }

    fun forCurrentCompany() = QProductEntity.productEntity.companyId.eq(CompanyContext.companyId.value)

}