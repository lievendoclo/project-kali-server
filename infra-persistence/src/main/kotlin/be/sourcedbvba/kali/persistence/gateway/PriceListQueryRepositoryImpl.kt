package be.sourcedbvba.kali.persistence.gateway

import be.sourcedbvba.kali.context.CompanyContext
import be.sourcedbvba.kali.domain.entity.PriceList
import be.sourcedbvba.kali.domain.exception.EntityNotFound
import be.sourcedbvba.kali.domain.gateway.PriceListQueryRepository
import be.sourcedbvba.kali.identifier.PriceListId
import be.sourcedbvba.kali.persistence.jpa.mapping.QPriceListEntity
import be.sourcedbvba.kali.persistence.jpa.repository.PriceListEntityRepository
import org.springframework.stereotype.Repository

@Repository
class PriceListQueryRepositoryImpl(private val priceListEntityRepository: PriceListEntityRepository) : PriceListQueryRepository {
    private val PRICELIST = QPriceListEntity.priceListEntity

    override fun getPriceList(id: PriceListId): PriceList {
        fun withId(id: PriceListId) = PRICELIST.id.eq(id.value)
        return priceListEntityRepository.findOne(forCurrentCompany().and(withId(id)))
                .orElseThrow { EntityNotFound() }
                .toDomain()
    }

    override fun findAllPriceLists(): List<PriceList> {
        return priceListEntityRepository.findAll(forCurrentCompany())
                .map { it.toDomain() }
    }

    fun forCurrentCompany() = PRICELIST.companyId.eq(CompanyContext.companyId.value)

}