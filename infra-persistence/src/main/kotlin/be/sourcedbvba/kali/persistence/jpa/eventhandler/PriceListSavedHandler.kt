package be.sourcedbvba.kali.persistence.jpa.eventhandler

import be.sourcedbvba.kali.domain.event.DomainEventConsumer
import be.sourcedbvba.kali.domain.event.PriceListSaved
import be.sourcedbvba.kali.persistence.jpa.eventhandler.annotation.EventHandler
import be.sourcedbvba.kali.persistence.jpa.mapping.toEntity
import be.sourcedbvba.kali.persistence.jpa.repository.PriceListEntityRepository

@EventHandler
class PriceListSavedHandler(val priceListEntityRepository: PriceListEntityRepository) : DomainEventConsumer<PriceListSaved> {
    override fun consume(event: PriceListSaved) {
        priceListEntityRepository.save(event.priceList.toEntity())
    }
}