package be.sourcedbvba.kali.persistence.jpa.eventhandler

import be.sourcedbvba.kali.domain.event.DomainEventConsumer
import be.sourcedbvba.kali.domain.event.TableAddedToFloor
import be.sourcedbvba.kali.identifier.FloorId
import be.sourcedbvba.kali.persistence.jpa.eventhandler.annotation.EventHandler
import be.sourcedbvba.kali.persistence.jpa.mapping.FloorEntity
import be.sourcedbvba.kali.persistence.jpa.repository.FloorEntityRepository
import be.sourcedbvba.kali.persistence.jpa.repository.TableEntityRepository

@EventHandler
class TableAddedToFloorHandler(val tableEntityRepository: TableEntityRepository,
                               val floorEntityRepository: FloorEntityRepository) : DomainEventConsumer<TableAddedToFloor> {
    override fun consume(event: TableAddedToFloor) {
        val floorDocument = event.floorId.toFloorEntity()
        floorDocument.addTable(event)
    }

    fun FloorEntity.addTable(tableAddedToFloor: TableAddedToFloor) {
        val tableDocument = tableEntityRepository.getOne(tableAddedToFloor.tableId.value)
        tables.add(tableDocument)
        floorEntityRepository.save(this)
    }

    fun FloorId.toFloorEntity() = floorEntityRepository.getOne(value)
}