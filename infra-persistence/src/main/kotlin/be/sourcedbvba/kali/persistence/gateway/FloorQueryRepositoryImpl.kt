package be.sourcedbvba.kali.persistence.gateway

import be.sourcedbvba.kali.context.CompanyContext
import be.sourcedbvba.kali.domain.entity.Floor
import be.sourcedbvba.kali.domain.exception.EntityNotFound
import be.sourcedbvba.kali.domain.gateway.FloorQueryRepository
import be.sourcedbvba.kali.identifier.FloorId
import be.sourcedbvba.kali.persistence.jpa.mapping.QFloorEntity
import be.sourcedbvba.kali.persistence.jpa.repository.FloorEntityRepository
import org.springframework.stereotype.Repository

@Repository
class FloorQueryRepositoryImpl(private val floorEntityRepository: FloorEntityRepository) : FloorQueryRepository {
    private val FLOOR = QFloorEntity.floorEntity

    override fun getFloorById(id: FloorId): Floor {
        fun withId(id: FloorId) = FLOOR.id.eq(id.value)

        return floorEntityRepository.findOne(forCurrentCompany().and(withId(id)))
                .orElseThrow { EntityNotFound() }
                .toDomain()
    }

    override fun findAllFloors(): Iterable<Floor> {
        return floorEntityRepository.findAll(forCurrentCompany()).map { it.toDomain() }
    }

    private fun forCurrentCompany() = FLOOR.companyId.eq(CompanyContext.companyId.value)

}