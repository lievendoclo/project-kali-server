package be.sourcedbvba.kali.persistence.jpa.mapping

import be.sourcedbvba.kali.context.CompanyContext
import be.sourcedbvba.kali.domain.entity.PriceList
import be.sourcedbvba.kali.identifier.CompanyId
import be.sourcedbvba.kali.identifier.PriceListId
import be.sourcedbvba.kali.identifier.ProductId
import com.sun.org.apache.bcel.internal.generic.LSTORE
import java.io.Serializable
import java.math.BigDecimal
import javax.money.Monetary
import javax.persistence.*

@Entity(name = "PriceList")
class PriceListEntity(@Id val id: String,
                      val companyId: String,
                      val name: String,
                      val currency: String,
                      @OneToMany(cascade = arrayOf(CascadeType.ALL)) @JoinColumn(name = "priceListId") val prices: List<PriceListPriceEntity>) : Serializable {
    fun toDomain(): PriceList {
        return PriceList(PriceListId(id), name, Monetary.getCurrency(currency), prices.groupBy { it.productId }.mapKeys { ProductId(it.key) }
                .mapValues { Monetary.getDefaultAmountFactory().setCurrency(currency).setNumber(it.value.first().price).create() })
    }
}

fun PriceList.toEntity() : PriceListEntity {
    return PriceListEntity(id.value, CompanyContext.companyId.value, name, currency.currencyCode, prices.map { PriceListPriceEntity(it.key.value, it.value.number.numberValue(BigDecimal::class.java)) })
}

@Entity(name = "PriceListPrice")
class PriceListPriceEntity(@Id val productId: String,
                           val price: BigDecimal) : Serializable
