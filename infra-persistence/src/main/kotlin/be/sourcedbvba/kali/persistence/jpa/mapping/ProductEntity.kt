package be.sourcedbvba.kali.persistence.jpa.mapping

import be.sourcedbvba.kali.context.CompanyContext
import be.sourcedbvba.kali.domain.entity.Product
import be.sourcedbvba.kali.identifier.CompanyId
import be.sourcedbvba.kali.identifier.ProductId
import javax.persistence.Entity
import javax.persistence.Id

@Entity(name = "Product")
data class ProductEntity(@Id val id: String,
                         val companyId: String,
                         val name: String) {
    fun toDomain(): Product {
        return Product(ProductId(id), name)
    }
}

fun Product.toEntity() : ProductEntity {
    return ProductEntity(id.value, CompanyContext.companyId.value, name);
}

