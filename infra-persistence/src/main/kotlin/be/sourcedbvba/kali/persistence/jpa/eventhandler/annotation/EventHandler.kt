package be.sourcedbvba.kali.persistence.jpa.eventhandler.annotation

import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional

@Target(allowedTargets = AnnotationTarget.CLASS)
@Component
@Transactional
annotation class EventHandler