package be.sourcedbvba.kali.persistence.jpa.repository

import be.sourcedbvba.kali.persistence.jpa.mapping.FloorEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.querydsl.QuerydslPredicateExecutor

interface FloorEntityRepository : JpaRepository<FloorEntity, String>, QuerydslPredicateExecutor<FloorEntity> {
}