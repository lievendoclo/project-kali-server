package be.sourcedbvba.kali.persistence.jpa.eventhandler

import be.sourcedbvba.kali.domain.event.DomainEventConsumer
import be.sourcedbvba.kali.domain.event.ProductSaved
import be.sourcedbvba.kali.persistence.jpa.eventhandler.annotation.EventHandler
import be.sourcedbvba.kali.persistence.jpa.mapping.toEntity
import be.sourcedbvba.kali.persistence.jpa.repository.ProductEntityRepository
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional

@EventHandler
class ProductSavedHandler(val productEntityRepository: ProductEntityRepository) : DomainEventConsumer<ProductSaved> {
    override fun consume(event: ProductSaved) {
        productEntityRepository.save(event.product.toEntity())
    }

}