package be.sourcedbvba.kali.persistence.jpa.mapping

import be.sourcedbvba.kali.context.CompanyContext
import be.sourcedbvba.kali.domain.entity.Table
import be.sourcedbvba.kali.identifier.CompanyId
import be.sourcedbvba.kali.identifier.TableId
import javax.persistence.Entity
import javax.persistence.Id

@Entity(name = "TableData")
class TableEntity(@Id val id: String,
                  val companyId: String,
                  val name: String) {
    fun toDomain(): Table {
        return Table(TableId(id), name)
    }
}

fun Table.toEntity() : TableEntity {
    return TableEntity(id.value, CompanyContext.companyId.value, name);
}