package be.sourcedbvba.kali.springboot

import be.sourcedbvba.kali.usecase.annotation.UseCase
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.FilterType


@Configuration
@ComponentScan(basePackages = arrayOf("be.sourcedbvba.kali"),
        includeFilters = arrayOf(ComponentScan.Filter(value = UseCase::class, type = FilterType.ANNOTATION)))
class Wiring {
}