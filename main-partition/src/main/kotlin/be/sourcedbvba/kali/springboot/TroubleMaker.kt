package be.sourcedbvba.kali.springboot

import com.keyholesoftware.troublemaker.client.EnableTroubleMaker
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile

@Configuration
@EnableTroubleMaker
@Profile("troublemaker")
class TroubleMaker {
}