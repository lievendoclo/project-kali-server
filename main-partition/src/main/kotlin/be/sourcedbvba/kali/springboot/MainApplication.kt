package be.sourcedbvba.kali.springboot

import org.slf4j.LoggerFactory
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class MainApplication {
    init {
        Thread.setDefaultUncaughtExceptionHandler { _, exception ->
            LoggerFactory.getLogger(MainApplication::class.java).error("Uncaught runtime exception: " + exception.message, exception)
        }
    }
}

fun main(args: Array<String>) {
    SpringApplication.run(MainApplication::class.java)

}