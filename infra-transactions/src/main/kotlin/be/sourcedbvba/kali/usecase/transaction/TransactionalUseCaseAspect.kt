package be.sourcedbvba.kali.usecase.transaction

import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.Around
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.annotation.Pointcut
import java.util.function.Supplier

@Aspect
class TransactionalUseCaseAspect(private val transactionalUseCaseExecutor: TransactionalUseCaseExecutor) {

    @Pointcut("within(@be.sourcedbvba.kali.usecase.annotation.UseCase *)")
    fun useCase() {
    }

    @Around("useCase()")
    fun useCase(proceedingJoinPoint: ProceedingJoinPoint): Any? {
        return transactionalUseCaseExecutor.executeInTransaction(Supplier { proceedingJoinPoint.proceed() })
    }
}
