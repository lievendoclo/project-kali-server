package be.sourcedbvba.kali.web

import be.sourcedbvba.kali.identifier.ProductId
import be.sourcedbvba.kali.usecase.CreateProduct
import be.sourcedbvba.kali.usecase.FindProducts
import be.sourcedbvba.kali.usecase.UpdateProduct
import io.restassured.module.mockmvc.RestAssuredMockMvc
import io.restassured.module.mockmvc.RestAssuredMockMvc.given
import io.restassured.module.mockmvc.RestAssuredMockMvc.mockMvc
import org.hamcrest.CoreMatchers
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito.doReturn
import org.mockito.Mockito.verify
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc

@ExtendWith(SpringExtension::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@ContextConfiguration(classes = arrayOf(WebConfig::class))
@EnableAutoConfiguration
@AutoConfigureMockMvc
internal class ProductRestControllerTest() {
    @MockBean
    lateinit var findProducts: FindProducts
    @MockBean
    lateinit var createProduct: CreateProduct
    @MockBean
    lateinit var updateProduct: UpdateProduct
    @Autowired
    lateinit var mockMvc: MockMvc

    @BeforeEach
    fun initializeRestAssured() {
        RestAssuredMockMvc.requestSpecification
        mockMvc(mockMvc)
    }

    @Test
    fun `Find products containing a part of the name`() {
        doReturn(listOf(FindProducts.Response(ProductId("1"), "test")))
                .`when`(findProducts).perform(FindProducts.Request("t"))

        given()
                .header("Accept", MediaType.APPLICATION_JSON_VALUE)
                .header("Company", "1")
                .`when`()
                .get("/services/product?nameContains=t")
                .then()
                .body("$.size()", CoreMatchers.equalTo(1))
                .body("[0].name", CoreMatchers.equalTo("test"))
                .body("[0].id", CoreMatchers.equalTo("1"))
    }

    @Test
    fun `Find products without parameters`() {
        doReturn(listOf(FindProducts.Response(ProductId("1"), "test")))
                .`when`(findProducts).perform(FindProducts.Request(null))

        given()
                .header("Accept", MediaType.APPLICATION_JSON_VALUE)
                .header("Company", "1")
                .`when`()
                .get("/services/product")
                .then()
                .body("$.size()", CoreMatchers.equalTo(1))
                .body("[0].name", CoreMatchers.equalTo("test"))
                .body("[0].id", CoreMatchers.equalTo("1"))
    }

    @Test
    fun `Create a product`() {
        doReturn(CreateProduct.Response(ProductId("1")))
                .`when`(createProduct).perform(CreateProduct.Request("test"))

        given()
                .header("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                .header("Company", "1")
                .body(mapOf("name" to "test"))
                .`when`()
                .post("/services/product")
                .then()
                .statusCode(201)
                .header("Location", "/1")
    }

    @Test
    fun `Update a product`() {
        given()
                .header("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                .header("Company", "1")
                .body(mapOf("name" to "test"))
                .`when`()
                .post("/services/product/1")
                .then()
                .statusCode(200)

        verify(updateProduct).perform(UpdateProduct.Request(ProductId("1"), "test"))
    }

}