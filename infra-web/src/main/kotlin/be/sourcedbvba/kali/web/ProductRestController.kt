package be.sourcedbvba.kali.web

import be.sourcedbvba.kali.identifier.ProductId
import be.sourcedbvba.kali.usecase.CreateProduct
import be.sourcedbvba.kali.usecase.FindProducts
import be.sourcedbvba.kali.usecase.UpdateProduct
import be.sourcedbvba.kali.web.model.ProductJson
import be.sourcedbvba.kali.web.model.toJsonList
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.net.URI


@RestController
@RequestMapping("/services/product")
class ProductRestController(private val createProduct: CreateProduct,
                            private val findProducts: FindProducts,
                            private val updateProduct: UpdateProduct) {

    @PostMapping
    fun createProduct(@RequestBody createProductJsonRequest: CreateProductJsonRequest): ResponseEntity<Unit> {
        val response = createProduct.perform(createProductJsonRequest.toRequest())
        val location = URI("/${response.id.value}")
        return ResponseEntity.created(location).build()
    }

    @GetMapping
    fun findProducts(params: FindProductsRequestParams): List<ProductJson> {
        return findProducts.perform(params.toRequest()).toJsonList()
    }

    @PutMapping
    @RequestMapping("/{productId}")
    fun updateProduct(@PathVariable productId: String, @RequestBody updateProductJsonRequest: UpdateProductJsonRequest): ResponseEntity<Unit> {
        updateProduct.perform(updateProductJsonRequest.toRequest(productId))
        return ResponseEntity(HttpStatus.OK)
    }


    data class CreateProductJsonRequest(val name: String) {
        fun toRequest(): CreateProduct.Request {
            return CreateProduct.Request(name)
        }
    }

    data class UpdateProductJsonRequest(val name: String) {
        fun toRequest(productId: String): UpdateProduct.Request {
            return UpdateProduct.Request(ProductId(productId), name)
        }
    }

    open class FindProductsRequestParams(var nameContains: String?) {
        fun toRequest() = FindProducts.Request(nameContains)
    }
}
