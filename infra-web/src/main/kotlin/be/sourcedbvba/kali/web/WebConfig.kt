package be.sourcedbvba.kali.web

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer


@Configuration
@ComponentScan
class WebConfig : WebMvcConfigurer {
    override fun addInterceptors(registry: InterceptorRegistry) {
        registry.addInterceptor(CompanyInterceptor())
    }

    //    @Bean
//    fun companyFilter(): FilterRegistrationBean<CompanyInterceptor> {
//        val filterRegistrationBean = FilterRegistrationBean<CompanyInterceptor>()
//        filterRegistrationBean.urlPatterns = listOf("/services/*")
//        filterRegistrationBean.filter = CompanyInterceptor()
//        return filterRegistrationBean
//    }
}
