package be.sourcedbvba.kali.web.model;

import be.sourcedbvba.kali.usecase.FindProducts

data class ProductJson(val id: String, val name: String)

fun List<FindProducts.Response>.toJsonList() = map { ProductJson(it.id.value, it.name) }