package be.sourcedbvba.kali.web

import be.sourcedbvba.kali.context.CompanyContext
import be.sourcedbvba.kali.identifier.CompanyId
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class CompanyInterceptor : HandlerInterceptorAdapter() {
    override fun preHandle(request: HttpServletRequest, response: HttpServletResponse, handler: Any?): Boolean {
        val companyId = request.getHeader("Company")
        CompanyContext.companyId = CompanyId(companyId)
        return true
    }
}
