package be.sourcedbvba.kali.domain.event.spring

import be.sourcedbvba.kali.domain.event.EventPublisher
import org.springframework.beans.factory.InitializingBean
import org.springframework.stereotype.Component

class EventPublisherHolderPopulator(private val eventPublisher: EventPublisher) : InitializingBean  {
    override fun afterPropertiesSet() {
        EventPublisher.Locator.eventPublisher = eventPublisher
    }
}
