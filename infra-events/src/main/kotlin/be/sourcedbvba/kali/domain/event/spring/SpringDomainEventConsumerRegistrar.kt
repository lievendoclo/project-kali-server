package be.sourcedbvba.kali.domain.event.spring

import be.sourcedbvba.kali.domain.event.DomainEvent
import be.sourcedbvba.kali.domain.event.DomainEventConsumer
import org.springframework.beans.factory.InitializingBean
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationListener
import org.springframework.context.PayloadApplicationEvent
import org.springframework.context.event.ApplicationEventMulticaster
import org.springframework.stereotype.Component
import kotlin.reflect.KClass

class SpringDomainEventConsumerRegistrar(val applicationContext: ApplicationContext,
                                         val applicationEventMulticaster: ApplicationEventMulticaster) : InitializingBean {
    override fun afterPropertiesSet() {
        val eventConsumers = getEventConsumers()
        eventConsumers.forEach(this::registerEventConsumer)
    }

    private fun registerEventConsumer(it: DomainEventConsumer<*>) {
        val klass = determineEventClass(it)
        applicationEventMulticaster.addApplicationListener(EventConsumerListener(klass, it))
    }

    private fun determineEventClass(eventConsumer: DomainEventConsumer<*>): KClass<*> {
        return eventConsumer::class.members.first { it.name == "consume" }.parameters.get(1).type.classifier as KClass<*>
    }


    fun getEventConsumers(): List<DomainEventConsumer<*>> {
        return applicationContext.getBeansOfType(DomainEventConsumer::class.java).values.toList()
    }

    class EventConsumerListener<E : DomainEvent>(val klass: KClass<*>, val domainEventConsumer: DomainEventConsumer<E>) :
            ApplicationListener<PayloadApplicationEvent<E>> {
        override fun onApplicationEvent(event: PayloadApplicationEvent<E>?) {
            if(klass.isInstance(event!!.payload)) {
                domainEventConsumer.consume(event.payload)
            }
        }
    }
}
