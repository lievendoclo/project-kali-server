package be.sourcedbvba.kali.domain.event.spring

import be.sourcedbvba.kali.domain.event.EventPublisher
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Component

class SpringApplicationEventPublisher(private val applicationEventPublisher: ApplicationEventPublisher) : EventPublisher {
    override fun publishEvent(event: Any) {
        applicationEventPublisher.publishEvent(event);
    }
}
