package be.sourcedbvba.kali.domain.event.spring

import be.sourcedbvba.kali.domain.event.EventPublisher
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.event.ApplicationEventMulticaster

@Configuration
class DomainEventConfiguration {
    @Configuration
    class Spring {
        @Bean
        fun domainEventConsumerRegistrar(applicationContext: ApplicationContext,
                                               applicationEventMulticaster: ApplicationEventMulticaster) = SpringDomainEventConsumerRegistrar(applicationContext, applicationEventMulticaster)

        @Bean
        fun applicationEventPublisher(applicationEventPublisher: ApplicationEventPublisher) = SpringApplicationEventPublisher(applicationEventPublisher)

    }

    @Bean
    fun domainEventLogger() = DomainEventLogger()

    @Bean
    fun eventPublisherHolderPopulator(eventPublisher: EventPublisher) = EventPublisherHolderPopulator(eventPublisher)
}