package be.sourcedbvba.kali.domain.event.spring

import be.sourcedbvba.kali.domain.event.DomainEvent
import org.slf4j.LoggerFactory
import org.springframework.context.event.EventListener

class DomainEventLogger {
    val logger = LoggerFactory.getLogger("domainevents")

    @EventListener
    fun logEvent(domainEvent: DomainEvent) {
        logger.debug(domainEvent.toString())
    }
}