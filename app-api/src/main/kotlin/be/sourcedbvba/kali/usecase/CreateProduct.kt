package be.sourcedbvba.kali.usecase

import be.sourcedbvba.kali.identifier.ProductId

interface CreateProduct {
    fun perform(request: Request) : Response

    data class Request(val name: String)

    data class Response(val id: ProductId)
}


