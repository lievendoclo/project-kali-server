package be.sourcedbvba.kali.usecase

import be.sourcedbvba.kali.identifier.ProductId
import be.sourcedbvba.kali.identifier.ReceiptId
import be.sourcedbvba.kali.identifier.TableId

interface SendOrderForTable {
    fun perform(request: Request) : Response

    class Request(val tableId: TableId,
                  val orderItems: List<OrderItem>) {
        data class OrderItem(val productId: ProductId,
                             val amount: Int)
    }

    class Response(val receiptId: ReceiptId)
}