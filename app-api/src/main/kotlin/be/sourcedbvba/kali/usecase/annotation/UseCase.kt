package be.sourcedbvba.kali.usecase.annotation

@Target(allowedTargets = AnnotationTarget.CLASS)
annotation class UseCase
