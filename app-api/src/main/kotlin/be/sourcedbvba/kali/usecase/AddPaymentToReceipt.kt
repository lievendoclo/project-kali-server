package be.sourcedbvba.kali.usecase

import be.sourcedbvba.kali.enums.PaymentType
import be.sourcedbvba.kali.identifier.PaymentId
import be.sourcedbvba.kali.identifier.ReceiptId
import javax.money.MonetaryAmount

interface AddPaymentToReceipt {
    fun perform(request: Request) : Response

    data class Request(val receiptId: ReceiptId,
                  val paymentType: PaymentType,
                  val amount: MonetaryAmount)

    data class Response(val id: PaymentId)
}
