package be.sourcedbvba.kali.usecase

import be.sourcedbvba.kali.identifier.ReceiptId

interface FinalizeReceipt {
    fun perform(request: Request)

    data class Request(val receiptId: ReceiptId)
}