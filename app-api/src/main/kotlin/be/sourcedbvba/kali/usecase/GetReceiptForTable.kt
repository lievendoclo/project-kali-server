package be.sourcedbvba.kali.usecase

import be.sourcedbvba.kali.identifier.ReceiptId
import be.sourcedbvba.kali.identifier.TableId

interface GetReceiptForTable {
    fun perform(request: Request) : Response

    data class Request(val tableId: TableId)

    data class Response(val receiptId: ReceiptId)
}
