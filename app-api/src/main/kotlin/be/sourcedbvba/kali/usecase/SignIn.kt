package be.sourcedbvba.kali.usecase

interface SignIn {
    fun perform(request: Request)

    class Request(val username: String,
                  val password: String)
}
