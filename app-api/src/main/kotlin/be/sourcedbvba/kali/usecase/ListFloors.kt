package be.sourcedbvba.kali.usecase

interface ListFloors {
    fun perform(request: Request) : Response

    class Request

    class Response
}