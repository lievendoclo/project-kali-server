package be.sourcedbvba.kali.usecase

import be.sourcedbvba.kali.identifier.ReceiptId

interface PrintReceipt {
    fun perform(request: Request)

    class Request(val receiptId: ReceiptId)
}