package be.sourcedbvba.kali.usecase

import be.sourcedbvba.kali.identifier.ProductGridId

interface CreateProductGrid {
    fun perform(request: Request) : Response

    data class Request(val name: String,
                       val rows: Int,
                       val columns: Int)

    data class Response(val id: ProductGridId)
}