package be.sourcedbvba.kali.usecase

import be.sourcedbvba.kali.identifier.ProductGridId
import be.sourcedbvba.kali.identifier.ProductId

interface AddProductToProductGrid {
    fun perform(request: Request)

    data class Request(val productGridId: ProductGridId,
                       val productId: ProductId,
                       val row: Int,
                       val column: Int)

}