package be.sourcedbvba.kali.usecase

import be.sourcedbvba.kali.identifier.ProductId

interface UpdateProduct {
    fun perform(request: Request)

    data class Request(val id: ProductId, val name: String)
}