package be.sourcedbvba.kali.usecase

import be.sourcedbvba.kali.identifier.FloorId
import be.sourcedbvba.kali.identifier.TableId

interface AddTableToFloor {
    fun perform(request: Request) : Response

    data class Request(val name: String,
                       val floorId: FloorId)

    data class Response(val id: TableId)
}