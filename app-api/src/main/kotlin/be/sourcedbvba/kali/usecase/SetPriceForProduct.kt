package be.sourcedbvba.kali.usecase

import be.sourcedbvba.kali.identifier.PriceListId
import be.sourcedbvba.kali.identifier.ProductId
import javax.money.MonetaryAmount

interface SetPriceForProduct {
    fun perform(request: Request)

    class Request(val priceListId: PriceListId,
                  val productId: ProductId,
                  val price: MonetaryAmount)
}
