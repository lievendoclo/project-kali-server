package be.sourcedbvba.kali.usecase

import be.sourcedbvba.kali.identifier.PriceListId
import javax.money.CurrencyUnit

interface CreatePriceList {
    fun perform(request: Request) : Response

    data class Request(val name: String,
                       val currency: CurrencyUnit)

    data class Response(val id: PriceListId)
}