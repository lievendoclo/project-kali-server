package be.sourcedbvba.kali.usecase

import be.sourcedbvba.kali.identifier.UserId

interface CreateUser {
    fun perform(request: Request) : Response

    data class Request(val name: String,
                  val username: String,
                  val password: String)

    data class Response(val id: UserId)
}