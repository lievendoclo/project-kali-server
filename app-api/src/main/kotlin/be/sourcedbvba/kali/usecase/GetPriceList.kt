package be.sourcedbvba.kali.usecase

import be.sourcedbvba.kali.identifier.PriceListId
import be.sourcedbvba.kali.identifier.ProductId
import javax.money.CurrencyUnit
import javax.money.MonetaryAmount

interface GetPriceList {
    fun perform(request: Request) : Response

    data class Request(val id: PriceListId)

    data class Response(val id: PriceListId,
                        val name: String,
                        val currency: CurrencyUnit,
                        val priceListItems: List<PriceListItem>)

    data class PriceListItem(val productId: ProductId,
                             val productName: String,
                             val price: MonetaryAmount)
}