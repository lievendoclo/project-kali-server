package be.sourcedbvba.kali.usecase

import be.sourcedbvba.kali.identifier.FloorId

interface CreateFloor {
    fun perform(request: Request) : Response

    data class Request(val name: String)

    data class Response(val id: FloorId)
}