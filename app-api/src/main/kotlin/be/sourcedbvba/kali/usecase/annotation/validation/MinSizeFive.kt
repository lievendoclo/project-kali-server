package be.sourcedbvba.kali.usecase.annotation.validation

import javax.validation.Constraint
import javax.validation.Payload
import kotlin.reflect.KClass

@Constraint(validatedBy = arrayOf())
annotation class MinSizeFive(
        val message : String = "be.sourcedbvba.kali.usecase.annotation.validation.MinSizeFive.message",
        val groups: Array<KClass<Any>> = arrayOf(),
        val payload: Array<KClass<Payload>> = arrayOf())
