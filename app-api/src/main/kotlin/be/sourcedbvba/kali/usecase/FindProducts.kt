package be.sourcedbvba.kali.usecase

import be.sourcedbvba.kali.identifier.ProductId

interface FindProducts {
    fun perform(request: Request): List<Response>

    data class Request(val nameContains: String?)

    data class Response(val id: ProductId, val name: String)
}






