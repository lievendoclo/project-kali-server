package be.sourcedbvba.kali.usecase.impl

import be.sourcedbvba.kali.domain.entity.Floor
import be.sourcedbvba.kali.usecase.CreateFloor
import be.sourcedbvba.kali.usecase.annotation.UseCase

@UseCase
class CreateFloorImpl : CreateFloor {
    override fun perform(request: CreateFloor.Request): CreateFloor.Response {
        val floor = Floor.createNew(request.name).save()
        return CreateFloor.Response(floor.id)
    }
}