package be.sourcedbvba.kali.usecase.impl

import be.sourcedbvba.kali.domain.entity.PriceList
import be.sourcedbvba.kali.domain.entity.Product
import be.sourcedbvba.kali.domain.gateway.PriceListQueryRepository
import be.sourcedbvba.kali.domain.gateway.ProductQueryRepository
import be.sourcedbvba.kali.identifier.ProductId
import be.sourcedbvba.kali.usecase.GetPriceList
import be.sourcedbvba.kali.usecase.annotation.UseCase

@UseCase
class GetPriceListImpl(val priceListQueryRepository: PriceListQueryRepository,
                       val productQueryRepository: ProductQueryRepository) : GetPriceList {
    override fun perform(request: GetPriceList.Request): GetPriceList.Response {
        return priceListQueryRepository.getPriceList(request.id).toResponseModel()
    }

    fun PriceList.toResponseModel(): GetPriceList.Response {
        return GetPriceList.Response(id, name, currency, prices.map { GetPriceList.PriceListItem(it.key, it.key.toProduct().name, it.value) })
    }

    private fun ProductId.toProduct(): Product {
        return productQueryRepository.getProduct(this)
    }
}