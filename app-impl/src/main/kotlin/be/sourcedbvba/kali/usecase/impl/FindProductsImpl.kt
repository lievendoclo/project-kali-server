package be.sourcedbvba.kali.usecase.impl

import be.sourcedbvba.kali.domain.entity.Product
import be.sourcedbvba.kali.domain.gateway.FindProductsQuery
import be.sourcedbvba.kali.domain.gateway.ProductQueryRepository
import be.sourcedbvba.kali.usecase.FindProducts
import be.sourcedbvba.kali.usecase.annotation.UseCase

@UseCase
class FindProductsImpl(val productQueryRepository: ProductQueryRepository) : FindProducts {
    override fun perform(request: FindProducts.Request): List<FindProducts.Response> {
        return productQueryRepository.findProducts(FindProductsQuery(request.nameContains))
                .map { it.toUseCaseResponseModel() }
    }

    private fun Product.toUseCaseResponseModel() : FindProducts.Response {
        return FindProducts.Response(id, name)
    }
}




