package be.sourcedbvba.kali.usecase.impl

import be.sourcedbvba.kali.domain.entity.Table
import be.sourcedbvba.kali.domain.gateway.FloorQueryRepository
import be.sourcedbvba.kali.usecase.AddTableToFloor
import be.sourcedbvba.kali.usecase.annotation.UseCase

@UseCase
class AddTableToFloorImpl(private val floorQueryRepository: FloorQueryRepository) : AddTableToFloor {
    override fun perform(request: AddTableToFloor.Request): AddTableToFloor.Response {
        val floor = request.getFloor()
        val table = request.createNewTable()
        floor.addTable(table)
        return table.createResponse()
    }

    private fun Table.createResponse() = AddTableToFloor.Response(id)

    private fun AddTableToFloor.Request.createNewTable() =
            Table.createNew(name).save()

    private fun AddTableToFloor.Request.getFloor() =
            floorQueryRepository.getFloorById(floorId)
}