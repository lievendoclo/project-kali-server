package be.sourcedbvba.kali.usecase.impl

import be.sourcedbvba.kali.domain.entity.Product
import be.sourcedbvba.kali.usecase.CreateProduct
import be.sourcedbvba.kali.usecase.annotation.UseCase

@UseCase
class CreateProductImpl() : CreateProduct {
    override fun perform(request: CreateProduct.Request): CreateProduct.Response {
        val product = Product.createNew(request.name).save()
        return CreateProduct.Response(product.id)
    }
}