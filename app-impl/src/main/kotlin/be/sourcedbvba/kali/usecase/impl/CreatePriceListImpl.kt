package be.sourcedbvba.kali.usecase.impl

import be.sourcedbvba.kali.domain.entity.PriceList
import be.sourcedbvba.kali.usecase.CreatePriceList
import be.sourcedbvba.kali.usecase.annotation.UseCase

@UseCase
class CreatePriceListImpl : CreatePriceList {
    override fun perform(request: CreatePriceList.Request): CreatePriceList.Response {
        val priceList = PriceList.createNew(request.name, request.currency).save()
        return CreatePriceList.Response(priceList.id)
    }
}