package be.sourcedbvba.kali.usecase.impl

import be.sourcedbvba.kali.domain.entity.Product
import be.sourcedbvba.kali.domain.gateway.ProductQueryRepository
import be.sourcedbvba.kali.usecase.UpdateProduct
import be.sourcedbvba.kali.usecase.annotation.UseCase

@UseCase
class UpdateProductImpl(val productQueryRepository: ProductQueryRepository) : UpdateProduct {
    override fun perform(request: UpdateProduct.Request) {
        val oldProduct = productQueryRepository.getProduct(request.id)
        oldProduct.patch(request).save()
    }

    private fun Product.patch(updateProductRequest: UpdateProduct.Request): Product {
        return Product(id, updateProductRequest.name)
    }
}