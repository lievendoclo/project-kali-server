package be.sourcedbvba.kali.domain

import be.sourcedbvba.kali.identifier.*
import net.andreinc.mockneat.MockNeat
import net.andreinc.mockneat.abstraction.MockUnit
import net.andreinc.mockneat.abstraction.MockUnitBase
import java.util.function.Supplier
import javax.money.CurrencyUnit
import javax.money.Monetary
import javax.money.MonetaryAmount

class MockData {
    companion object {
        fun companyIds() = CompanyIds()
        fun productIds() = ProductIds()
        fun priceListIds() = PriceListIds()
        fun floorIds() = FloorIds()
        fun tableIds() = TableIds()

        fun currencyUnits() = MoneyApi.CurrencyUnits()
        fun monetaryAmounts(currencyUnit: CurrencyUnit) = MoneyApi.MonetaryAmounts(currencyUnit)
    }

    class CompanyIds : MockUnitBase(MockNeat.secure()), MockUnit<CompanyId> {
        override fun supplier(): Supplier<CompanyId> {
            return Supplier { CompanyId(mockNeat.strings().size(10).`val`()) }
        }
    }

    class PriceListIds : MockUnitBase(MockNeat.secure()), MockUnit<PriceListId> {
        override fun supplier(): Supplier<PriceListId> {
            return Supplier { PriceListId(mockNeat.strings().size(10).`val`()) }
        }
    }

    class ProductIds : MockUnitBase(MockNeat.secure()), MockUnit<ProductId> {
        override fun supplier(): Supplier<ProductId> {
            return Supplier { ProductId(mockNeat.strings().size(10).`val`()) }
        }
    }

    class FloorIds : MockUnitBase(MockNeat.secure()), MockUnit<FloorId> {
        override fun supplier(): Supplier<FloorId> {
            return Supplier { FloorId(mockNeat.strings().size(10).`val`()) }
        }
    }

    class TableIds : MockUnitBase(MockNeat.secure()), MockUnit<TableId> {
        override fun supplier(): Supplier<TableId> {
            return Supplier { TableId(mockNeat.strings().size(10).`val`()) }
        }
    }

    class MoneyApi {
        class CurrencyUnits : MockUnitBase(MockNeat.secure()), MockUnit<CurrencyUnit> {
            override fun supplier(): Supplier<CurrencyUnit> {
                return Supplier { Monetary.getCurrency(mockNeat.currencies().code().valStr()) }
            }
        }

        class MonetaryAmounts(val currencyUnit: CurrencyUnit = CurrencyUnits().`val`()) : MockUnitBase(MockNeat.secure()), MockUnit<MonetaryAmount> {
            override fun supplier(): Supplier<MonetaryAmount> {
                return Supplier { Monetary.getDefaultAmountFactory().setCurrency(currencyUnit).setNumber(mockNeat.doubles().range(0.0, 1000.0).`val`()).create() }
            }
        }
    }
}