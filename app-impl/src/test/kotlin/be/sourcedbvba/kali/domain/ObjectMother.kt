package be.sourcedbvba.kali.domain

import be.sourcedbvba.kali.domain.entity.Floor
import be.sourcedbvba.kali.domain.entity.PriceList
import be.sourcedbvba.kali.domain.entity.Product
import be.sourcedbvba.kali.domain.entity.Table
import net.andreinc.mockneat.MockNeat

class ObjectMother {
    companion object {
        private val mockNeat = MockNeat.secure()

        val SALAD_PRODUCT_NAME = mockNeat.strings().size(10).`val`()
        val SPAGHETTI_PRODUCT_NAME = mockNeat.strings().size(10).`val`()

        val ACME_COMPANY_ID = MockData.companyIds().`val`()
        val GLOBEX_COMPANY_ID = MockData.companyIds().`val`()

        val SALAD = Product(MockData.productIds().`val`(), SALAD_PRODUCT_NAME)
        val SPAGHETTI = Product(MockData.productIds().`val`(), SPAGHETTI_PRODUCT_NAME)

        val DEFAULT_CURRENCY = MockData.currencyUnits().`val`()
        val STANDARD_PRICELIST = PriceList(MockData.priceListIds().`val`(), mockNeat.strings().size(10).`val`(), DEFAULT_CURRENCY,
                mapOf(SALAD.id to MockData.monetaryAmounts(DEFAULT_CURRENCY).`val`()))

        val DOWNSTAIRS_FLOOR = Floor(MockData.floorIds().`val`(), mockNeat.strings().size(10).`val`(), mutableSetOf())
        val TABLE_ONE = Table(MockData.tableIds().`val`(), mockNeat.strings().size(10).`val`())
    }
}
