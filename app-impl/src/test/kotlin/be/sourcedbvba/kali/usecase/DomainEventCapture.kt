package be.sourcedbvba.kali.usecase

import be.sourcedbvba.kali.domain.event.DomainEvent
import org.springframework.context.event.EventListener

class DomainEventCapture {
    val events = mutableListOf<DomainEvent>()

    fun clearCapturedEvents() {
        events.clear()
    }

    @EventListener
    fun capture(domainEvent: DomainEvent) {
        events.add(domainEvent)
    }
}
