package be.sourcedbvba.kali.usecase

import be.sourcedbvba.kali.context.CompanyContext
import be.sourcedbvba.kali.domain.ObjectMother
import be.sourcedbvba.kali.domain.ObjectMother.Companion.DOWNSTAIRS_FLOOR
import be.sourcedbvba.kali.domain.gateway.FloorQueryRepository
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

internal class CreateFloorTest
@Autowired constructor(val createFloor: CreateFloor,
                       val floorQueryRepository: FloorQueryRepository) : UseCaseTest() {
    @Test
    fun create() {
        `Given I am working for the Acme company`()
        `When I create a downstairs floor`()
        `Then I can find a downstairs floor`()
    }
    private fun `Given I am working for the Acme company`() {
        CompanyContext.companyId = ObjectMother.ACME_COMPANY_ID
    }

    private fun `Then I can find a downstairs floor`() {
        val floors = floorQueryRepository.findAllFloors()
        Assertions.assertThat(floors).hasSize(1)
    }

    private fun `When I create a downstairs floor`() {
        createFloor.perform(CreateFloor.Request(DOWNSTAIRS_FLOOR.name))
        assertEventCount(1)
    }
}