package be.sourcedbvba.kali.usecase


import be.sourcedbvba.kali.context.CompanyContext
import be.sourcedbvba.kali.domain.ObjectMother
import be.sourcedbvba.kali.domain.ObjectMother.Companion.SALAD
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

internal class FindProductsTest
@Autowired constructor(val findProducts: FindProducts) : UseCaseTest() {
    @Test
    fun findAllProductsForACompany() {
        `Given I am working for the Acme company`()
        `Given there is a salad product`()
        val response = `When I search for the salad product name`()
        `Then I find the salad`(response)
    }

    @Test
    fun hidesProductsOfOtherCompanies() {
        `Given I am working for the Acme company`()
        `Given there is a salad product`()
        `If I change my company to Globex`()
        val response = `When I search for the salad product name`()
        `Then there are no products`(response)
    }

    private fun `If I change my company to Globex`() {
        CompanyContext.companyId = ObjectMother.GLOBEX_COMPANY_ID
    }

    private fun `Given I am working for the Acme company`() {
        CompanyContext.companyId = ObjectMother.ACME_COMPANY_ID
    }

    private fun `Then I find the salad`(response: List<FindProducts.Response>) {
        assertThat(response).hasSize(1)
        assertThat(response.filter { it.id == SALAD.id }).hasSize(1)
    }

    private fun `Then there are no products`(response: List<FindProducts.Response>) {
        assertThat(response).hasSize(0)
    }

    private fun `When I search for the salad product name`(): List<FindProducts.Response> {
        return findProducts.perform(FindProducts.Request(SALAD.name))
    }

    private fun `Given there is a salad product`() {
        SALAD.save()
    }
}