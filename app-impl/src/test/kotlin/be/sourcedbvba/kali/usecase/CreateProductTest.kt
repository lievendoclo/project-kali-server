package be.sourcedbvba.kali.usecase

import be.sourcedbvba.kali.context.CompanyContext
import be.sourcedbvba.kali.domain.ObjectMother
import be.sourcedbvba.kali.domain.ObjectMother.Companion.SPAGHETTI
import be.sourcedbvba.kali.domain.gateway.FindProductsQuery
import be.sourcedbvba.kali.domain.gateway.ProductQueryRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

internal class CreateProductTest
@Autowired constructor(val createProduct: CreateProduct,
                       val productQueryRepository: ProductQueryRepository) : UseCaseTest() {
    @Test
    fun create() {
        `Given I am working for the Acme company`()
        `When I create a spaghetti product`()
        `Then I can find a spaghetti product`()
    }

    private fun `Given I am working for the Acme company`() {
        CompanyContext.companyId = ObjectMother.ACME_COMPANY_ID
    }


    private fun `Then I can find a spaghetti product`() {
        val products = productQueryRepository.findProducts(FindProductsQuery(SPAGHETTI.name))
        assertThat(products).hasSize(1)
    }

    private fun `When I create a spaghetti product`() {
        createProduct.perform(CreateProduct.Request(SPAGHETTI.name))
        assertEventCount(1)
    }
}