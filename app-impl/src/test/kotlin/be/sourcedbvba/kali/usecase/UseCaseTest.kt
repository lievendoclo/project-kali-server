package be.sourcedbvba.kali.usecase

import be.sourcedbvba.kali.domain.event.spring.DomainEventConfiguration
import be.sourcedbvba.kali.persistence.PersistenceConfiguration
import be.sourcedbvba.kali.usecase.transaction.UseCaseTransactionConfiguration
import be.sourcedbvba.kali.usecase.validation.UseCaseValidationConfiguration
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit.jupiter.SpringExtension
import javax.transaction.Transactional

@ExtendWith(SpringExtension::class)
@SpringBootTest
@ContextConfiguration(classes = arrayOf(PersistenceConfiguration::class,
        UseCaseTestConfig::class,
        DomainEventConfiguration::class,
        UseCaseTransactionConfiguration::class,
        UseCaseValidationConfiguration::class))
@Transactional
internal class UseCaseTest() {
    @Autowired
    lateinit var domainEventCapture: DomainEventCapture

    @BeforeEach
    fun clearData() {
        domainEventCapture.clearCapturedEvents()
    }

    fun assertEventCount(count: Int) = assertThat(domainEventCapture.events.size).isEqualTo(count)

}