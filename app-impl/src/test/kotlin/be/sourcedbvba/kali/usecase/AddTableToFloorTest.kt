package be.sourcedbvba.kali.usecase

import be.sourcedbvba.kali.context.CompanyContext
import be.sourcedbvba.kali.domain.ObjectMother
import be.sourcedbvba.kali.domain.ObjectMother.Companion.DOWNSTAIRS_FLOOR
import be.sourcedbvba.kali.domain.ObjectMother.Companion.TABLE_ONE
import be.sourcedbvba.kali.domain.entity.Floor
import be.sourcedbvba.kali.domain.gateway.FloorQueryRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

internal class AddTableToFloorTest
@Autowired constructor(val addTableToFloor: AddTableToFloor,
                       val floorQueryRepository: FloorQueryRepository) : UseCaseTest() {
    @Test
    fun create() {
        `Given I am working for the Acme company`()
        var floor = `Given there is a downstairs floor`()
        `When I add a table to it`(floor)
        floor = `Then I can find a downstairs floor`()
        `And it has the table on it`(floor)
    }

    private fun `Given I am working for the Acme company`() {
        CompanyContext.companyId = ObjectMother.ACME_COMPANY_ID
    }

    private fun `When I add a table to it`(floor: Floor) {
        addTableToFloor.perform(AddTableToFloor.Request(TABLE_ONE.name, floor.id))
        assertEventCount(3)
    }

    private fun `And it has the table on it`(floor: Floor) {
        assertThat(floor.tables).hasSize(1)
    }

    private fun `Then I can find a downstairs floor`() : Floor {
        val floors = floorQueryRepository.findAllFloors()
        assertThat(floors).hasSize(1)
        return floors.first()
    }

    private fun `Given there is a downstairs floor`() : Floor {
        val floor = DOWNSTAIRS_FLOOR.save()
        assertEventCount(1)
        return floor
    }
}