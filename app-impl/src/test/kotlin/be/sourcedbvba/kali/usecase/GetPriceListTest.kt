package be.sourcedbvba.kali.usecase

import be.sourcedbvba.kali.context.CompanyContext
import be.sourcedbvba.kali.domain.ObjectMother
import be.sourcedbvba.kali.domain.ObjectMother.Companion.SALAD
import be.sourcedbvba.kali.domain.ObjectMother.Companion.STANDARD_PRICELIST
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

internal class GetPriceListTest
@Autowired constructor(val getPriceList: GetPriceList) : UseCaseTest() {
    @Test
    fun create() {
        `Given I am working for the Acme company`()
        `Given there is a pricelist with a product on it`()
        val priceList = `When I get that pricelist`()
        `Then the data on the pricelist is correct`(priceList)
    }

    private fun `Given I am working for the Acme company`() {
        CompanyContext.companyId = ObjectMother.ACME_COMPANY_ID
    }

    private fun `When I get that pricelist`() : GetPriceList.Response {
        return getPriceList.perform(GetPriceList.Request(STANDARD_PRICELIST.id))
    }

    private fun `Then the data on the pricelist is correct`(priceList: GetPriceList.Response) {
        assertThat(priceList.id).isEqualTo(STANDARD_PRICELIST.id)
        assertThat(priceList.name).isEqualTo(STANDARD_PRICELIST.name)
        assertThat(priceList.currency).isEqualTo(STANDARD_PRICELIST.currency)
        assertThat(priceList.priceListItems).hasSize(1)
        assertThat(priceList.priceListItems[0].productId).isEqualTo(SALAD.id)
        assertThat(priceList.priceListItems[0].price).isEqualTo(STANDARD_PRICELIST.prices[SALAD.id])
    }

    private fun `Given there is a pricelist with a product on it`() {
        SALAD.save()
        STANDARD_PRICELIST.save()
    }
}