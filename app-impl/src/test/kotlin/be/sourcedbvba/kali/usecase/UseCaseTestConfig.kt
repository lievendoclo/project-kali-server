package be.sourcedbvba.kali.usecase

import be.sourcedbvba.kali.usecase.annotation.UseCase
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan(basePackages = arrayOf("be.sourcedbvba.kali.usecase"),
        includeFilters = arrayOf(ComponentScan.Filter(value = UseCase::class)))
@EnableAutoConfiguration
class UseCaseTestConfig {
    @Bean
    fun domainEventCapture() = DomainEventCapture()
}