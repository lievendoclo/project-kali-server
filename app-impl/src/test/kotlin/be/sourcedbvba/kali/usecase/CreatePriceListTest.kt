package be.sourcedbvba.kali.usecase

import be.sourcedbvba.kali.context.CompanyContext
import be.sourcedbvba.kali.domain.ObjectMother
import be.sourcedbvba.kali.domain.ObjectMother.Companion.STANDARD_PRICELIST
import be.sourcedbvba.kali.domain.gateway.PriceListQueryRepository
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

internal class CreatePriceListTest
@Autowired constructor(val createPriceList: CreatePriceList,
                       val priceListQueryRepository: PriceListQueryRepository) : UseCaseTest() {
    @Test
    fun create() {
        `Given I am working for the Acme company`()
        `When I create a price list`()
        `Then I can find that price list`()
    }

    private fun `Given I am working for the Acme company`() {
        CompanyContext.companyId = ObjectMother.ACME_COMPANY_ID
    }

    private fun `Then I can find that price list`() {
        val priceLists = priceListQueryRepository.findAllPriceLists()
        Assertions.assertThat(priceLists).hasSize(1)
    }

    private fun `When I create a price list`() {
        createPriceList.perform(CreatePriceList.Request(STANDARD_PRICELIST.name, STANDARD_PRICELIST.currency))
        assertEventCount(1)
    }
}