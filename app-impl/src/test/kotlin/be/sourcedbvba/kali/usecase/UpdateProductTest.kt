package be.sourcedbvba.kali.usecase

import be.sourcedbvba.kali.context.CompanyContext
import be.sourcedbvba.kali.domain.ObjectMother
import be.sourcedbvba.kali.domain.ObjectMother.Companion.SALAD
import be.sourcedbvba.kali.domain.entity.Product
import be.sourcedbvba.kali.domain.exception.EntityNotFound
import be.sourcedbvba.kali.domain.gateway.FindProductsQuery
import be.sourcedbvba.kali.domain.gateway.ProductQueryRepository
import be.sourcedbvba.kali.identifier.ProductId
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

internal class UpdateProductTest
@Autowired constructor(val updateProduct: UpdateProduct,
                       val productQueryRepository: ProductQueryRepository): UseCaseTest() {

    val NEW_NAME = "Caesar salad"
    val NON_EXISTING_PRODUCT = Product(ProductId("-1"),"Kangaroo steak")

    @Test
    @DisplayName("Updating non-existing product")
    fun updateNonExisting() {
        `Given I am working for the Acme company`()
        assertThatThrownBy {
            `When I update the name of the product`(NON_EXISTING_PRODUCT)
        }.isInstanceOf(EntityNotFound::class.java)
    }

    @Test
    @DisplayName("Updating existing product")
    fun updateExisting() {
        `Given I am working for the Acme company`()
        val product = `Given there is a salad product`()
        `When I update the name of the product`(product)
        `Then I find a product with new product name`()
    }

    @Test
    @DisplayName("Updating existing product of another company fails")
    fun updateExistingOfAnotherCompany() {
        `Given I am working for the Acme company`()
        val product = `Given there is a salad product`()
        `I change my company to Globex`()
        assertThatThrownBy {
            `When I update the name of the product`(product)
        }.isInstanceOf(EntityNotFound::class.java)
    }

    private fun `Given I am working for the Acme company`() {
        CompanyContext.companyId = ObjectMother.ACME_COMPANY_ID
    }

    private fun `I change my company to Globex`() {
        CompanyContext.companyId = ObjectMother.GLOBEX_COMPANY_ID
    }

    private fun `Given there is a salad product`() : Product {
        return SALAD.save()
    }

    private fun `When I update the name of the product`(product: Product) {
        updateProduct.perform(UpdateProduct.Request(product.id, NEW_NAME))
        assertEventCount(2)
    }

    private fun `Then I find a product with new product name`() {
        val products = productQueryRepository.findProducts(FindProductsQuery(NEW_NAME))
        assertThat(products.map { it.name }).contains(NEW_NAME)
    }
}