package be.sourcedbvba.kali.usecase.validation

import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.Around
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.annotation.Pointcut
import javax.validation.ConstraintViolationException
import javax.validation.Validator

@Aspect
class UseCaseValidatonAspect(val validator: Validator) {

    @Pointcut("within(@be.sourcedbvba.kali.usecase.annotation.UseCase *)")
    fun useCase() {
    }

    @Around("useCase()")
    fun useCase(proceedingJoinPoint: ProceedingJoinPoint): Any? {
        if(proceedingJoinPoint.args.isNotEmpty()) {
            validateUseCaseArgument(proceedingJoinPoint.args[0]);
        }
        return proceedingJoinPoint.proceed()
    }

    private fun validateUseCaseArgument(arg: Any) {
        val validate = validator.validate(arg)
        if(validate.isNotEmpty()) {
            throw ConstraintViolationException(validate)
        }
    }
}
