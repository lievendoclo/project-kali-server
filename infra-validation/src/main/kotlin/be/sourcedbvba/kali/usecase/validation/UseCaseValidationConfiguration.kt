package be.sourcedbvba.kali.usecase.validation

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.EnableAspectJAutoProxy
import javax.validation.Validator

@Configuration
@EnableAspectJAutoProxy
class UseCaseValidationConfiguration {
    @Bean
    fun useCaseValidationAspect(validator: Validator) = UseCaseValidatonAspect(validator)
}