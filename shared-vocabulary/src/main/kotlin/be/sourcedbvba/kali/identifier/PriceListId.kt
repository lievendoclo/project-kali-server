package be.sourcedbvba.kali.identifier

data class PriceListId(val value: String)