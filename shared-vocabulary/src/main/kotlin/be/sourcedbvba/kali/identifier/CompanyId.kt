package be.sourcedbvba.kali.identifier

data class CompanyId(val value: String)