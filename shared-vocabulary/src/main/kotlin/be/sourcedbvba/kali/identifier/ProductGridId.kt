package be.sourcedbvba.kali.identifier

data class ProductGridId(val value: String)