package be.sourcedbvba.kali.identifier

data class TableId(val value: String)