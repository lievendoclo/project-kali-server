package be.sourcedbvba.kali.identifier

data class PaymentId(val value: String)