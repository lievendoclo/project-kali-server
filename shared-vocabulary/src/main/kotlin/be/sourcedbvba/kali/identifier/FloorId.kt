package be.sourcedbvba.kali.identifier

data class FloorId(val value: String)