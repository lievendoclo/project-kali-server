package be.sourcedbvba.kali.identifier

data class UserId(val value: String)