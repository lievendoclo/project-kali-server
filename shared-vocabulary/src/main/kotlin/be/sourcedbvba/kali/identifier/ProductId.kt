package be.sourcedbvba.kali.identifier

data class ProductId(val value: String)