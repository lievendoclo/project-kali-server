package be.sourcedbvba.kali.identifier

data class ReceiptId(val value: String)