package be.sourcedbvba.kali.context

import be.sourcedbvba.kali.identifier.CompanyId

object CompanyContext {
    private val companyIdThreadLocal: InheritableThreadLocal<CompanyId> = InheritableThreadLocal()

    var companyId: CompanyId
        get() = companyIdThreadLocal.get()
        set(value) = companyIdThreadLocal.set(value)
}
