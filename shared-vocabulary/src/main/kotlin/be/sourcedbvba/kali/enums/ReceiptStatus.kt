package be.sourcedbvba.kali.enums

enum class ReceiptStatus{
    OPEN,
    FINALIZED
}