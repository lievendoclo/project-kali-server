package be.sourcedbvba.kali.enums

enum class PaymentType {
    CASH,
    DEBIT_CARD,
    CREDIT_CARD
}